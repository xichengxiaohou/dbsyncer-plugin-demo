#### 介绍
本项目提供开发者编写[dbsyncer](https://gitee.com/ghi/dbsyncer)（简称dbs）插件示例

> 插件有什么用？

插件是一种可扩展全量同步和增量同步实现数据转换的技术方式。通过插件可以接收同步数据，自定义同步到目标源的行数据，也能消费数据并实现更多业务场景。

## 📦开发说明
dbsyncer-plugin-demo是基于[dbsyncer](https://gitee.com/ghi/dbsyncer)项目做插件开发，所以需要依赖开发包

#### 配置依赖包：
> 通过maven命令，安装依赖包到本地仓库
``` mvn
# 下载dbs项目
git clone https://gitee.com/ghi/dbsyncer.git

# 安装dbs开发包到maven本地仓库中（首次安装或升级版本后需要）
mvn install -Dmaven.test.skip=true
```

> 当前项目pom.xml已引入依赖，如需升级或更换版本可以手动替换dbsyncer.version版本号
``` pom
<!-- 全局参数版本 -->
<properties>
    <dbsyncer.version>1.2.3-RC_0427</dbsyncer.version>
</properties>

<dependency>
  <groupId>org.ghi</groupId>
  <artifactId>dbsyncer-common</artifactId>
  <version>${dbsyncer.version}</version>
</dependency>
<dependency>
  <groupId>org.ghi</groupId>
  <artifactId>dbsyncer-connector</artifactId>
  <version>${dbsyncer.version}</version>
</dependency>
```

## ⚙️手动打包
> 插件开发完成后，将本项目打包成jar文件

![输入图片说明](https://foruda.gitee.com/images/1682651895435505883/e1e1b015_376718.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1682651816357678152/fff7695e_376718.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1669738573957606706/a16962cc_376718.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1669738625485485240/10bef9b6_376718.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1669738663643499589/ba10de21_376718.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1669739614435716356/584a7931_376718.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1669739647156834353/4790b99f_376718.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1669739671362923799/d601301d_376718.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1669741905671222401/2eabc144_376718.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1669741936510194201/9aef9083_376718.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1669741987318795158/d343cdc3_376718.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1669739810336510454/7e46e787_376718.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1669739857010853387/eeca3a7f_376718.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1669739965983571780/20b85961_376718.png "屏幕截图")

## 🤝参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

## 💕了解更多
* 使用说明：[博客地址](https://my.oschina.net/dbsyncer "https://my.oschina.net/dbsyncer")（小提示：现在需要先登录，才能查看完整的教程信息，包含截图等😂）
* QQ群: 875519623或点击右侧按钮<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=fce8d51b264130bac5890674e7db99f82f7f8af3f790d49fcf21eaafc8775f2a"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="数据同步dbsyncer" title="数据同步dbsyncer" /></a>