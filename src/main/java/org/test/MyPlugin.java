package org.test;

import org.dbsyncer.common.spi.ConnectorMapper;
import org.dbsyncer.common.spi.ConvertContext;
import org.dbsyncer.common.spi.ConvertService;
import org.dbsyncer.connector.database.DatabaseConnectorMapper;
import org.dbsyncer.connector.database.ds.SimpleConnection;

public class MyPlugin implements ConvertService {

    /**
     * 全量同步/增量同步
     *
     * @param convertContext
     */
    @Override
    public void convert(ConvertContext convertContext) {
        // TODO 消费或处理数据
        System.out.println("插件消费数据中...");

        // 是否终止同步到目标库开关，默认false
        convertContext.setTerminated(false);

        // 获取Spring上下文，当然也可获取dbs注册的bean对象
        convertContext.getContext();

        // 数据源表和目标源表
        convertContext.getSourceTableName();
        convertContext.getTargetTableName();

        // 捕获的事件
        convertContext.getEvent();

        // 数据源和目标源表全量或增量数据
        convertContext.getSourceList();
        convertContext.getTargetList();

        // 获取目标库连接器实例
        convertContext.getTargetConnectorMapper();
    }

    /**
     * 全量同步/增量同步完成后执行处理
     *
     * @param context
     */
    @Override
    public void postProcessAfter(ConvertContext context) {
        // TODO 完成同步后调用该方法
        ConnectorMapper connectorMapper = context.getSourceConnectorMapper();

        // 获取关系型数据库连接，实现自己的业务逻辑...
        if (connectorMapper instanceof DatabaseConnectorMapper) {
            DatabaseConnectorMapper db = (DatabaseConnectorMapper) connectorMapper;
            // 方式一（推荐）：
            String query = "select * from my_user";
            db.execute(databaseTemplate -> databaseTemplate.queryForList(query));

            // 方式二：
            SimpleConnection connection = null;
            try {
                // 通过JDBC访问数据库
                connection = (SimpleConnection) db.getConnection();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if(connection != null){
                    connection.close();
                }
            }
        }
    }

    /**
     * 重写方法：设置版本号
     *
     * @return
     */
    @Override
    public String getVersion() {
        return "1.0.0";
    }

    /**
     * 重写方法：设置插件名称
     *
     * @return
     */
    @Override
    public String getName() {
        return "我的插件";
    }
}
